# generator-nette-component
> create new nette component

**required: node version 7.6+, npm**

## Installation

First, install [Yeoman](http://yeoman.io) and generator-nette-component using [npm](https://www.npmjs.com/) (assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g https://gitlab.com/alesmraz/yeoman-nette-component
```

Then generate your new project:

```bash
yo nette-component [componentName]
```
_note: component is auto capitalize_

select path to save from: Project, FrontModule, AdminModule

**Tests is auto generated too, don't forget to edit it too.**

## Update local global package

```
npm update -g generator-nette-component
```

## License

MIT © [Ales Mraz](alesmraz.cz)