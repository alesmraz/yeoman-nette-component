<?php

declare(strict_types = 1);

namespace <%= config.testNamespace %>;

use <%= config.namespace %><%= component %>\<%= component %>;
use Ant\Setting\SettingFacade;
use Testbench\TCompiledContainer;
use Testbench\TComponent;
use Tester\TestCase;

require dirname(__DIR__) . '/../../bootstrap.php';

/**
 * @testCase
 */
class <%= component %>Test extends TestCase
{

	use TCompiledContainer;
	use TComponent;

	/** @var SettingFacade */
	private $settingFacade;

	public function setUp()
	{
		$this->settingFacade = $this->getService(SettingFacade::class);
	}

	public function testRender(): void
	{
		$component = new <%= component %>($this->settingFacade);
		$this->checkRenderOutput($component, '<div class="<%= component %>">%A%');
	}
}

(new <%= component %>Test)->run();
